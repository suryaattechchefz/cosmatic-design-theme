* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

// font family
@import url("https://fonts.googleapis.com/css2?family=Allura&display=swap");
@import url("https://fonts.googleapis.com/css2?family=Allura&family=Montserrat&display=swap");
@import url("https://fonts.googleapis.com/css2?family=Allura&family=Cormorant:wght@500&family=Montserrat&display=swap");

//fonts
$font-montserrat: "Montserrat", sans-serif;
$font-cormorant: "Cormorant", serif;
$font-allura: "Allura", cursive;

// header-section styles starts here
.header-section {
  height: 130px;
  .header-logo {
    text-align: center;
    padding-top: 20px;
  }

  .header-nav {
    .header-navlist {
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
      margin-top: 40px;
      ul {
        font-family: $font-cormorant;
        li {
          a {
            font-size: 19px;
            color: #b8b8b8;
          }
        }
      }
    }

    .header-btn {
      a {
        text-decoration: none;
        color: black;
        .cart-btn{
          height:32px;
          width:30px;
          background: transparent;
          border:2px solid black;
          position: relative;
          font-size:14px;
          &::before{
            content:'';
            position:absolute;
            top:-9px;
            left:29%;
            height: 14px;
            width:12px;
            background: transparent;
            border:2px solid black;
            border-top-left-radius:10px;
            border-top-right-radius:10px;
            border-bottom:none;
          }
        }
      }
      .bar {
        display: none;
      }
    }
  }
}

// header section media queries starts here
@media (max-width: 768px) {
  .header-section {
    .header-nav {
      .header-navlist {
        display: none;
      }
      .header-btn {
        .bar {
          display: block;
        }
      }
    }
  }
}
// header section media queries ends here
// ** header section ends here **

// Cart section starts here
.cart-section {
  .cart-inner-section {
    padding: 100px 0;
    width: 85%;
    h1 {
      font-size: 30px;
      font-family: $font-montserrat;
    }
    .cart-text {
      padding-left: 20px;
      i {
        margin-right: 20px;
      }
      margin-top: 25px;
      border: none;
      border-top: 2px solid #515151;
      padding: 15px 0;
      font-family: $font-cormorant;
      font-size: 19px;
      text-transform: capitalize;
      color: #515151;
    }

    button {
      margin-top: 30px;
      width: 250px;
      padding: 10px 10px;
      background: #000;
      color: white;
      outline: none;
      border: none;
      cursor: pointer;
      font-family: $font-montserrat;
      letter-spacing: 2px;
      font-size: 19px;
      font-weight: bold;
    }
  }
}
// Cart section ends here

// cart item styles starts here
.cart-details-section {
  .cart-details-inner {
    width: 83%;
    h1 {
      font-family: $font-montserrat;
      font-size: 30px;
    }

    .row {
      padding-bottom: 50px;
      .table-one {
        table {
          width: 100%;
          tr {
            th {
              padding: 10px 15px;
              font-family: $font-cormorant;
              color: black;
              font-weight: lighter;
              font-size: 19px;
            }

            td {
              padding: 10px 15px;
              font-family: $font-cormorant;
              color: black;
              font-size: 19px;
              img {
                width: 70px;
                height: 70px;
                object-fit: contain;
              }
            }
          }
        }

        .input-btn {
          margin-top: 20px;
          input {
            width: 50%;
            padding: 7px 5px;
            outline: none;
            border: 1px solid lightgrey;
            font-family: $font-cormorant;
            font-size: 17px;
          }

          button {
            width: 50%;
            padding: 8px 0;
            font-family: $font-montserrat;
            font-weight: bold;
            letter-spacing: 2px;
            background: #000;
            color: white;
            outline: none;
            border: none;
          }
        }
      }

      .table-two {
        table {
          width: 100%;
          tr {
            th {
              padding: 10px 15px;
              font-family: $font-cormorant;
              color: black;
              font-weight: lighter;
              font-size: 19px;
            }

            td {
              padding: 29px 15px;
              font-family: $font-cormorant;
              color: black;
              font-size: 19px;
              img {
                width: 70px;
                height: 70px;
                object-fit: contain;
              }
            }
          }
        }

        .btn {
          margin-top: 18px;
          padding: 0;
          width: 100%;
          button {
            width: 40%;
            padding: 9px 0;
            font-family: $font-montserrat;
            font-weight: bold;
            letter-spacing: 2px;
            background: #000;
            color: white;
            outline: none;
            border: none;
          }
        }
      }
    }

    .cart-total-sec {
      width: 100%;
      margin-top: 30px;
      .col {
        .cart-total {
          width: 48%;
          h1 {
            font-family: $font-montserrat;
            font-size: 30px;
            margin-bottom: 20px;
          }

          table {
            width: 100% !important;

            tr {
              font-family: $font-cormorant;
              font-size: 19px;
              th {
                padding: 10px 35px;
                font-weight: lighter;
              }

              td {
                padding: 10px 35px;
              }
            }
          }

          .cart-total-btn {
            margin-top: 20px;
            width: 100%;
            button {
              width: 100%;
              padding: 15px 5px;
              background: #000;
              color: white;
              font-family: $font-montserrat;
              font-weight: bold;
              letter-spacing: 2px;
              outline: none;
              border: none;
              font-size: 20px;
            }
          }
        }
      }
    }
  }
}

@media (max-width: 768px) {
  input {
    width: 100% !important;
    margin: 20px 0;
  }

  button {
    width: 100% !important;
  }

  .cart-total-sec {
    width: 100% !important;
    margin-left: 0;
    .col {
      .cart-total {
        width: 100% !important;
        table {
          width: 100% !important;
          padding: 0 !important;
        }
      }
    }
  }
}
// Footer section starts here
.footer-section {
  padding: 50px 0;
  margin-top: 50px;
  background: #000;
  .footer-inner-section {
    width: 85%;
    margin: auto;
    .row {
      .col-md-4 {
        h1 {
          font-family: $font-montserrat;
          font-size: 27px !important;
          padding-bottom: 20px;
          text-align: left;
          text-transform: uppercase;
        }
        p {
          text-align: left;
          font-family: $font-cormorant;
          font-size: 19px;
          line-height: 40px;
        }
      }

      .right {
        h1 {
          font-family: $font-montserrat;
          font-size: 27px;
        }
        ul li {
          font-family: $font-cormorant;
          font-size: 19px;
          margin: 15px 0;
        }
      }
    }
  }
}

// Footer section styles ends here

// foorer social icons styles starts here
.footer-socialicons {
  font-family: $font-cormorant;
  .col-md-6 {
    font-size: 19px;
    letter-spacing: 1px;
    padding-bottom: 10px;
  }
}
// Footer social icons styles ends here
